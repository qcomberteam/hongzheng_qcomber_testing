/**
 * @author hongzhen
 *
 * State Machine Enumeration for UI
 * 
 */
public interface MenuStateMachine {
	public final static int MAIN_MENU = 0;
	public final static int CREATE_ACCOUNT = 1;
	public final static int LOGIN = 2;
	public final static int ACCOUNT_MANAGEMENT = 3;
	public final static int ADD_DEVICE = 4;
	public final static int DELETE_DEVICE = 5;
	public final static int MANAGE_DEVICE = 6;
	public final static int DEVICE_ACTION = 7;
	
	public final static int ABOUT_QCOMBER = 9;
}
