/**
 * @author hongzhen
 * This is a psudo-database used to test the Qcomber back-end system
 */

import java.util.*;

public class DB {

//	public DB() {
//	}
	

	// psudo-database info:
	private Map<String,User> emailUserMap = new HashMap<String,User>();
	private Map<User, List<Device>> userDevicesMap = new HashMap<User, List<Device>>();
	private Map<User, List<Rule>> userRulesMap = new HashMap<User, List<Rule>>();
	
	public User addUser(String email, User user) {
		userDevicesMap.put(user, new ArrayList<Device>()); // initialize Device DB for each user
		userRulesMap.put(user, new ArrayList<Rule>());	// initialize Rule DB for each user
		return emailUserMap.put(email,user); // add user to user DB
	}
	
	public User getUser(String email) {
		return emailUserMap.get(email);
	}
	
	
	public List<Device> getDevicesList(String email) {
		return userDevicesMap.get(emailUserMap.get(email));
	}
	
	public boolean addDevice(String email, Device device) {
		return userDevicesMap.get(emailUserMap.get(email)).add(device);
	}
	
	public boolean deleteDevice(String email, Device device) {
		return userDevicesMap.get(emailUserMap.get(email)).remove(device);
	}
	
	
	public List<Rule> getRulesList(String email) {
		return userRulesMap.get(emailUserMap.get(email));
	}
	
	public boolean addRule(String email, Rule rule) {
		return userRulesMap.get(emailUserMap.get(email)).add(rule);
	}
	
	public boolean deleteRule(String email, Rule rule) {
		return userRulesMap.get(emailUserMap.get(email)).remove(rule);
	}

}
