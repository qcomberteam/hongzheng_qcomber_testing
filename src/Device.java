/**
 * @author hongzhen
 * 
 * Device class is an abstract class. It represents a device model (a device "type"):
 * The brand and the specific model are compulsory fields, e.g. Apple - iPhone 6 Plus.
 * 
 * A device model sometimes comes with a model ID, e.g. iPhone 6 Plus sold in North America 
 * all come with Model ID: A1522 (represents the operational frequency). 
 * Note that this field should be set by an administrator for user's convenience.
 * 
 * Also optionally, the user can set the type of his/her devices by putting his/her iPhone 5 
 * and iPhone 6 Plus under "Cell Phones": this information is stored under the field "deviceType"
 * (we may also change this field to be set by administrator only.)
 * 
 * All Devices have the following abstract methods to be defined specifically in subclasses:
 * 1) turnOn() - turn on the device
 * 2) turnOff() - turn off the device
 * 3) checkBattery() - check the battery status of the device
 * 4) isActivated() - check whether the device is activated
 * 
 */

import java.util.*;

public abstract class Device {
	// Device class represents a certain device model, i.e. iPhone 4S, Philips HUE LED.
	
	private DeviceModel deviceModel;
	
	// The following are optional parameters
	private String deviceType; // e.g. Cell Phones
	private String deviceModelID; // e.g. A1522
	
	private List<API> APIs = new ArrayList<API>();
	
	/**
	 * Constructor when only DeviceModel is given
	 * @param deviceModel
	 */
	public Device(DeviceModel deviceModel, List<API> apis) {
		setDeviceModel(deviceModel);
		setAPIs(apis);
	}
	
	/**
	 * Constructor when the optional type is given by user
	 * @param deviceModel
	 * @param type
	 */
	public Device(DeviceModel deviceModel, List<API> apis, String type) {
		setDeviceModel(deviceModel);
		setAPIs(apis);
		setDeviceType(type);
	}

	public DeviceModel getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(DeviceModel deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceModelID() {
		return deviceModelID;
	}

	public void setDeviceModelID(String deviceModelID) {
		this.deviceModelID = deviceModelID;
	}	

	public List<API> getAPIs() {
		return APIs;
	}

	public void setAPIs(List<API> APIs) {
		this.APIs = APIs;
	}
	
	public abstract DeviceModel returnDeviceModel();
	public abstract boolean turnOn();
	public abstract boolean turnOff();
	public abstract int checkBattery();
	public abstract boolean isActivated();

}
