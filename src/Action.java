/**
 * @author hongzhen
 * 
 * An action on a Device, i.e. an API call on a Device.
 * Action object is specific to a Device.
 * e.g. turn on William's iPhone 6 Plus, or turn on William's Panasonic microwave. 
 * 
 */
public class Action {

	private API deviceAPI;
	private Device onDevice;
	
	/**
	 * Constructor
	 * @param API
	 * @param device
	 */
	public Action(API API, Device device) {
		setDeviceAPI(API);
		setOnDevice(device);
	}

	public API getDeviceAPI() {
		return deviceAPI;
	}

	public void setDeviceAPI(API deviceAPI) {
		this.deviceAPI = deviceAPI;
	}

	public Device getOnDevice() {
		return onDevice;
	}

	public void setOnDevice(Device onDevice) {
		this.onDevice = onDevice;
	}

}
