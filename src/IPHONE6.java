/**
 * IPHONE6 object is a specific iPhone6 with its serial ID provided by user
 * @author hongzhen
 *
 */

import java.util.*;

public class IPHONE6 extends Device implements MobileDevice {
	
	private String deviceSerialID;

	public IPHONE6(DeviceModel deviceModel, String serialID, List<API> apis) {
		super(deviceModel, apis);
		deviceSerialID = serialID;
	}
	
	public IPHONE6(DeviceModel deviceModel, String serialID, List<API> apis, String type) {
		super(deviceModel, apis, type);
		deviceSerialID = serialID;
	}

	public String getDeviceSerialID() {
		return deviceSerialID;
	}

	// this setter is only used by admin when user entered an incorrect serial ID  
	public void setDeviceSerialID(String deviceSerialID) {
		this.deviceSerialID = deviceSerialID;
	}

	@Override
	public boolean turnOnBluetooth() {
		System.out.println("Turning on bluetooth of the device with serial ID: " + 
							deviceSerialID);
//		API();
		return true;
	}

	@Override
	public boolean turnOffBluetooth() {
		System.out.println("Turning off bluetooth of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public int[] getGPSLocation() {
		int[] GPSLocation = {50,50};
		System.out.println("Returning the GPS location of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return GPSLocation;
	}

	@Override
	public boolean turnOn() {
		System.out.println("Turning on the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public boolean turnOff() {
		System.out.println("Turning off the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public int checkBattery() {
		System.out.println("Returning the battery condition of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return 50;
	}

	@Override
	public boolean isActivated() {
		System.out.println("Returning the activation status of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public DeviceModel returnDeviceModel() {
		return DeviceModel.APPLE_IPHONE6;
	}

}
