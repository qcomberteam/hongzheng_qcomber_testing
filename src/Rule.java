/**
 * Rule object represents a specific rule
 * A rule object contains an action based on a list of condition(s):
 * 		(1) if there is a single condition: then the list of operator(s) should be empty, the
 *          action is triggered once the condition is met.
 * 		(2) if there are multiple conditions: then the conditions are connected according to the
 * 			list of operator(s), the action is triggered when the connected conditions are met.
 * 
 * e.g.: William's rule: Turn on my HUE LED when time is after 7:00am on Aug 1, 2015 and temperature is 25 oC 
 *
 * @author hongzhen
 */

import java.util.*;

public class Rule {
	
	private List<Condition> conditionList = new ArrayList<Condition>();
	private List<String> operatorList = new ArrayList<String>(); 
	// An operator is normally "&&" or "||", thus can in fact be Enum or Boolean
	private Action ruleAction;
	
	// The following is/are optional parameter(s)
	private String ruleName; 
	
	/**
	 * Constructor when user does not give the nickname of the rule:
	 * @param conditionList
	 * @param operatorList
	 * @param action
	 */
	public Rule(List<Condition> conditionList, List<String> operatorList, Action action) {
		setConditionList(conditionList);
		setOperatorList(operatorList);
		setRuleAction(action);
	}
	
	/**
	 * Constructor when user gives the nickname of the rule:
	 * @param conditionList
	 * @param operatorList
	 * @param action
	 * @param name
	 */
	public Rule(List<Condition> conditionList, List<String> operatorList, Action action, String name) {
		setConditionList(conditionList);
		setOperatorList(operatorList);
		setRuleAction(action);
		setRuleName(name);
	}

	public List<Condition> getConditionList() {
		return conditionList;
	}

	public void setConditionList(List<Condition> conditionList) {
		this.conditionList = conditionList;
	}

	public List<String> getOperatorList() {
		return operatorList;
	}

	public void setOperatorList(List<String> operatorList) {
		this.operatorList = operatorList;
	}

	public Action getRuleAction() {
		return ruleAction;
	}

	public void setRuleAction(Action ruleAction) {
		this.ruleAction = ruleAction;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

}
