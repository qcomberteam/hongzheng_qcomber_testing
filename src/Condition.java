/**
 * Condition object consists of the ConditionType and the Parameter:
 * ConditionType: e.g. temperature higher than, time later than, Arrived at school, etc.
 * 				  Note that ConditionType could be implemented by String, but making it 
 * 				  a separate class makes it easier for management.
 * Parameter: e.g. 25 degrees Celsius (for temperature related ConditionType), 21:34 (Time related), etc.
 * 			  OR 
 * 				   null when parameter is not needed (e.g. Arrived home)  
 * 
 * @author hongzhen
 */

public class Condition {
	// Device class represents a specific device, i.e. William's Black iPhone 6 Plus

	private ConditionType myConditionType;
	//private String myConditionType;
	private String myParameter;
	
	/**
	 * Constructor for the Condition Class
	 * @param conditionType
	 * @param parameter
	 */
	public Condition(ConditionType conditionType, String parameter) {
		setMyConditionType(conditionType);
		setMyParameter(parameter);
	}

	public ConditionType getMyConditionType() {
		return myConditionType;
	}

	public void setMyConditionType(ConditionType myConditionType) {
		this.myConditionType = myConditionType;
	}

	public String getMyParameter() {
		return myParameter;
	}

	public void setMyParameter(String myParameter) {
		this.myParameter = myParameter;
	}

}
