import java.util.List;

/**
 * HUELED object is a specific Philips HUE LED with its serial ID provided by user
 * @author hongzhen
 *
 */
public class HUELED extends Device implements LightingDevice {

	private String deviceSerialID;

	public HUELED(DeviceModel deviceModel, String serialID, List<API> apis) {
		super(deviceModel, apis);
		deviceSerialID = serialID;
	}
	
	public HUELED(DeviceModel deviceModel, String serialID, List<API> apis, String type) {
		super(deviceModel, apis, type);
		deviceSerialID = serialID;
	}

	public String getDeviceSerialID() {
		return deviceSerialID;
	}

	// this setter is only used by admin when user entered an incorrect serial ID  
	public void setDeviceSerialID(String deviceSerialID) {
		this.deviceSerialID = deviceSerialID;
	}
	
	@Override
	public int[] changeColor(int[] RGB) {
		System.out.println("Set color of the device with serial ID: " + 
				deviceSerialID + "to RGB: " + RGB[0] + ", " + RGB[1] + ", " + RGB[2] );
//		API();
		return RGB;
	}

	@Override
	public int changeBrightness(int brightness) {
		System.out.println("Set brightness of the device with serial ID: " + 
				deviceSerialID + "to " + brightness + "%.");
//		API();
		return brightness;
	}

	@Override
	public int autoTurnOff(int time) {
		System.out.println("Auto turn off the device with serial ID: " + 
				deviceSerialID + "in " + time + " minutes.");
//		API();
		return time;
	}

	@Override
	public boolean turnOn() {
		System.out.println("Turning on the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public boolean turnOff() {
		System.out.println("Turning off the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}

	@Override
	public int checkBattery() {
		System.out.println("Returning the battery condition of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return 50;
	}

	@Override
	public boolean isActivated() {
		System.out.println("Returning the activation status of the device with serial ID: " + 
				deviceSerialID);
//		API();
		return true;
	}
	
	@Override
	public DeviceModel returnDeviceModel() {
		return DeviceModel.PHILIPS_HUELED;
	}

}
