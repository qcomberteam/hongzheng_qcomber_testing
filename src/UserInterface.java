import acm.program.*;

import java.util.*;

/**
 * @author hongzhen
 * This is a user interface used to test the Qcomber back-end system
 */
@SuppressWarnings("serial")
public class UserInterface extends ConsoleProgram implements MenuStateMachine  {
	
	public void run() {
		setSize(800,600); pause(100); // set screen size
		
		println("Welcome to the qcomber system.");
		state = MAIN_MENU;
		
		while (true) {

			switch (state) {
			case MAIN_MENU: {
				mainMenuInit();	// main menu printout
				state = readInt(); // get user input
				if ( !isValidInput() ) { //check whether user has entered valid input
					println("Invalid input!"); pause(1000);
					println("Going back to main menu...");
					state = MAIN_MENU; pause(1000); break; 
				}
				inputList.clear(); break;
			}
				
			case CREATE_ACCOUNT: {
				createAccountInit();
				String str = "Please enter your email address:\n";
				String email = getNonEmptyInput(str + "(Enter 0 to go back to main menu) ");
				
				if (email.equals("0")) { // if user enter 0 to go back to main menu
					println("Going back to main menu..."); state = MAIN_MENU; pause(1000); break;
				}
				if ( DBIO.getUser(email) != null) { // if there has been such an account already
					println("Account associated with " + email + " already exists.");
					println("Going back to main menu..."); state = MAIN_MENU; pause(1000); break;
				}
				
				// ask user to set password
				String password = userSetPassword(email); // the user needs to enter two same passwords
				if (password == null) { // if user fails to set password
					println("Going back to main menu..."); state = MAIN_MENU; pause(1000); break;
				}
				
				User newUser = new User(email, password);
				
				println("The account " + email + " has been created successfully!"); pause(1000);
				println("If you want to set a nickname on your account, please enter now:");
				String nickname = readLine("(if you do not need a nickname, enter 0) ");
				
				if (!(nickname.equals("0"))) {
					newUser.setUserNickname(nickname);
					println("Nickname \"" + nickname + "\" set to account " + email + ".");
				}

				DBIO.addUser(email, newUser);
				emailPasswordMap.put(email,password); // for testing's convenience
				
				println("Going back to main menu..."); state = MAIN_MENU; pause(1000); break;
			}
				
			case LOGIN: {
				loginInit();
				String str = "Please enter your email address:\n";
				String email = getNonEmptyInput(str);
				
				while ( DBIO.getUser(email) == null) { // when no such account
					println("There is no account associated with " + email); pause(1000);
					String str2 = "(enter 1 to create new account, 0 to go back to main menu) ";
					email = getNonEmptyInput(str + str2);
					
					if (email.equals("1")) {
						state = CREATE_ACCOUNT; 
						break;
					} else if (email.equals("0")) {
						state = MAIN_MENU; 
						break;
					}
				}
				if (state != LOGIN) break;
				
				println("Please enter the password associated with " + email + ":");
				String password = readLine();
				
				if ( !(emailPasswordMap.get(email).equals(password)) ) {
					println("Login failed. Email and password entered do not match!"); pause(1000); 
					break;
				}
				
				loginAccount = email; // set current login account
				
				println("Login successful..."); state = ACCOUNT_MANAGEMENT; pause(1000); 
				break;
			}
					
			case ACCOUNT_MANAGEMENT: {
				accountManagementInit();
				state = readInt();
				if ( !isValidInput() ) { //check whether user has entered valid input
					println("Invalid input!");
					state = ACCOUNT_MANAGEMENT; pause(1000); break;
				}
				
				switch (state) {
				case (4): // List of devices
					List<Device> devicesList = DBIO.getDevicesList(loginAccount);
					displayDevicesList(devicesList);
					state = ACCOUNT_MANAGEMENT; inputList.clear(); pause(1000); break;
					
				case (5): // Logout
					loginAccount = null;
					println("Logout successfully..."); 
					println("Going back to main menu...");
					state = MAIN_MENU; inputList.clear(); pause(1000); break;
					
				default:
					state += ACCOUNT_MANAGEMENT;
				}
				
				inputList.clear(); break;
			}
			
			case ADD_DEVICE: {
				// Since will return to Account Management regardless of input,
				//addDeviceInit() itself does everything needed.
				addDeviceInit(); 
				state = ACCOUNT_MANAGEMENT; pause(1000); break;
			}
			
			case DELETE_DEVICE: {
				// Since will return to Account Management regardless of input,
				// deleteDeviceInit() itself does everything needed.
				deleteDeviceInit(); 
				state = ACCOUNT_MANAGEMENT; pause(1000); break;
			}
			
			case MANAGE_DEVICE: {
				manageDeviceInit();
				state = ACCOUNT_MANAGEMENT; pause(1000); break;
			}
			
			case ABOUT_QCOMBER: {
				aboutQcomberInit();
				state = MAIN_MENU; pause(1000); break;
			}
					
			default:
				println("Incorrect input!");
				
				if (loginAccount != null) {
					state = ACCOUNT_MANAGEMENT;
					println("Going back to Account Management...");
				} else {
					state = MAIN_MENU;
					println("Going back to Main Menu...");
				}
				pause(1000);
			}
			
		}
		
	}
	
	private boolean isValidInput() {
		if ( !inputList.contains(state) ) {
			inputList.clear();
			return false;
		}
		return true;
	}
	
	private String getNonEmptyInput(String message) {
		/**
		 * This function guarantees a non-empty input String entered by a user
		 * message will be displayed to the screen
		 */
		
		String input = readLine(message);
		
		while (input.length() == 0) {
			println("Oh, come on. Enter something man.");
			input = readLine(message);
		}
		
		return input;
	}
	
	private String getNonEmptyInput() {
		/**
		 * This function guarantees a non-empty input String entered by a user
		 * this function does not display message
		 */
		
		String input = readLine();
		
		while (input.length() == 0) {
			println("Oh, come on. Enter something man.");
			input = readLine();
		}
		
		return input;
	}
	
	private String userSetPassword(String email) {
		/**
		 * This function returns the confirmed user-set password.
		 * A user has 2 chances to set the password (password and password_confirm match).
		 * If the user fails to set the password by entering two same passwords,
		 * the confirmation fails and null is returned.
		 */
		
		String password = getNonEmptyInput("Please set the password associated with " + email + ":\n");
		String password_confirm = getNonEmptyInput("Please confirm the password:\n");
		
		if ( !(password_confirm.equals(password)) ) { // passwords not matching for the 1st time
			println("Password confirmation failed -- passwords do not match."); pause(1000);		
			password = getNonEmptyInput("Please set the password associated with " + email + ":\n");
			password_confirm = getNonEmptyInput("Please confirm the password:\n");
			
			if ( !(password_confirm.equals(password)) ) { // passwords not matching for the 2nd time
				println("Password confirmation failed again. Seriously?!! :)"); 
				return null; // set password to be null for the switch in run() to break
			}
		}
		
		return password;
	}
	
	private void mainMenuInit() {
		println("\n----------Main Menu----------");
		println("Please select one of the following:");
		println("1. Create account");
		println("2. Login");
		println("9. About Qcomber");
		inputList.add(1); inputList.add(2); inputList.add(9);
	}
	private void createAccountInit() {
		println("\n----------Create Account----------");
	}
	private void loginInit() {
		println("\n----------Login----------");
	}
	private void accountManagementInit() {
		println("\n----------Account Management: " + loginAccount + "----------");
		println("Please select one of the following:");
		println("1. Add device");
		println("2. Delete device");
		println("3. Manage device");
		println("4. List of devices");
		println("5. Logout");
		inputList.add(1); inputList.add(2); inputList.add(3); inputList.add(4); inputList.add(5);
	}
	
	private void addDeviceInit() {
		println("\n----------Add Device: " + loginAccount + "----------");
		println("Please select one of the following devices you want to add:");
		println("1. Apple iPhone 6");
		println("2. Philips Hue LED");
		int userInput = readInt("(Enter other value to go back) ");
		
		String serialID; String brand; String model; DeviceModel deviceModel; 
		List<API> APIs = new ArrayList<API>();
		
		switch (userInput) {
		case 1:
			serialID = getSerialID();
			brand = "Apple";
			model = "iPhone 6";
			deviceModel = DeviceModel.APPLE_IPHONE6;
			deviceModel.setBrand(brand);
			deviceModel.setModel(model);
			APIs.add(new API(deviceModel, "Turn on")); APIs.add(new API(deviceModel, "Turn off"));
			APIs.add(new API(deviceModel, "Check battery")); APIs.add(new API(deviceModel, "Check activation status"));
			APIs.add(new API(deviceModel, "Turn on bluetooth")); APIs.add(new API(deviceModel, "Turn off bluetooth"));
			APIs.add(new API(deviceModel, "Get GPS location"));
			IPHONE6 iphone6 = new IPHONE6(deviceModel, serialID, APIs);
			DBIO.addDevice(loginAccount, iphone6);
			println("New " + brand + " " + model + " added to " + loginAccount + " successfully.");
			break;
		case 2:
			serialID = getSerialID();
			brand = "Philips";
			model = "Hue LED";
			deviceModel = DeviceModel.PHILIPS_HUELED;
			deviceModel.setBrand(brand);
			deviceModel.setModel(model);
			APIs.add(new API(deviceModel, "turnOn")); APIs.add(new API(deviceModel, "turnOff"));
			APIs.add(new API(deviceModel, "Check battery")); APIs.add(new API(deviceModel, "Check activation status"));
			APIs.add(new API(deviceModel, "Change color")); APIs.add(new API(deviceModel, "Change brightness"));
			APIs.add(new API(deviceModel, "Set time to turn off"));
			HUELED hueled = new HUELED(deviceModel, serialID, APIs);
			DBIO.addDevice(loginAccount, hueled);
			println("New " + brand + " " + model + " added to " + loginAccount + " successfully.");
			break;
		default:
			println("No device added. Going back...");
		}	
	}
	private void deleteDeviceInit() {
		println("\n----------Delete Device: " + loginAccount + "----------");
		List<Device> devicesList = DBIO.getDevicesList(loginAccount);
		int numDevices = displayDevicesList(devicesList);
		if (numDevices == 0) { // if no device in list
			println("No device deleted. Going back...");
			return;
		}
		println("Please select one of the above devices you want to delete:");
		int userInput = readInt("(Enter other value to go back) ");
		
		if( !(userInput>0 && userInput<=numDevices) ) { // if invalid input
			println("No device deleted. Going back...");
			return;
		} else {
			Device device = devicesList.get(userInput-1);
			DBIO.deleteDevice(loginAccount, device); // delete device from list
			String brand = device.getDeviceModel().getBrand();
			String model = device.getDeviceModel().getModel();
			println(brand + " " + model + " deleted from your account.");
		}
	}
	private void manageDeviceInit() {
		println("\n----------Manage Device: " + loginAccount + "----------");
		List<Device> devicesList = DBIO.getDevicesList(loginAccount);
		int numDevices = displayDevicesList(devicesList);
		if (numDevices == 0) { // if no device in list
			println("No device to manage. Going back...");
			return;
		}
		println("Please select one of the above devices you want to manage:");
		int userInput = readInt("(Enter other value to go back) ");
		
		if( !(userInput>0 && userInput<=numDevices) ) { // if invalid input
			println("No device managed. Going back...");
			return;
		} else {
			Device device = devicesList.get(userInput-1);
 			println("Please select one of the following available actions:");
			int i = 0;
			for (API api: device.getAPIs()) {
				println((++i) + ". " + api.getDeviceAPI());
			}
			userInput = readInt("(Enter other value to go back) ");
			if (!(userInput>0 && userInput<=i)) { // if invalid input, go back
				println("No device managed. Going back...");
				return;
			} else {
				String brand; String model;
				switch (device.returnDeviceModel()) {
				case APPLE_IPHONE6:
					switch (userInput) {
					case 1: // turn on device
						((IPHONE6) device).turnOn();
						break;
					case 2: // turn off
						((IPHONE6) device).turnOff();
						break;
					case 3: // check battery
						((IPHONE6) device).checkBattery();
						break;
					case 4: // check whether device is activated
						((IPHONE6) device).isActivated();
						break;
					case 5: // turn on bluetooth
						((IPHONE6) device).turnOnBluetooth();
						break;
					case 6: // turn off bluetooth
						((IPHONE6) device).turnOffBluetooth();
						break;
					case 7: // get GPS location
						((IPHONE6) device).getGPSLocation();
						break;
					}
					break;
				case PHILIPS_HUELED:
					switch (userInput) {
					case 1: // turn on
						((HUELED) device).turnOn();
						break;
					case 2: // turn off
						((HUELED) device).turnOff();
						break;
					case 3: // check battery
						((HUELED) device).checkBattery();
						break;
					case 4: // check whether device is activated
						((HUELED) device).isActivated();
					case 5: // change color to RGB (int[]);
						int R = readInt("Please set red value (0~255): ");
						int G = readInt("Please set red value (0~255): ");
						int B = readInt("Please set red value (0~255): ");
						int[] RGB = {R,G,B};
						((HUELED) device).changeColor(RGB);
						break;
					case 6: // change brightness to BN%
						int BN = readInt("Please set brightness value in % (0~100): ");
						((HUELED) device).changeBrightness(BN);
						break;
					case 7: // automatically turn off Hue LED in time minutes
						int time = readInt("Please set the time to turn off the device in minutes.");
						((HUELED) device).autoTurnOff(time);
					}
					break;
				default:
					;	
				}
			}
		}
	}
	private void aboutQcomberInit() {
		println("\n----------About Qcomber----------");
		println("So you seriously thought I wrote some company cultrue here ya? Go back man...");
	}
	
	private int displayDevicesList(List<Device> devicesList) {
		if (devicesList.isEmpty()) {
			println("There is no device in your account.");
			return 0;
		} else {
			println("The following are your " + devicesList.size() + " devices:");
			int i = 0;
			for (Device device: devicesList) {
				println((++i) + ". " + device.getDeviceModel().getBrand() + " " + device.getDeviceModel().getModel());
			}
			return devicesList.size();
		}
	}
	
	private String getSerialID() {
		String str1 = "Please enter the serial ID of the device:\n";
		String str2 = "(usually found at the back of the device) ";
		String serialID = getNonEmptyInput(str1 + str2);
		
		return serialID;
	}

	private int state; // please refer to MenuStatteMachine for the state machine info.
	private List<Integer> inputList = new ArrayList<Integer>(); // check for valid user input in each state
	private String loginAccount; // current login account

	private DB DBIO = new DB();
	public Map<String,String> emailPasswordMap = new HashMap<String,String>();
}
