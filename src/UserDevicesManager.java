/**
 * UserDevicesManager object belongs to a specific User.
 * UserDevicesManager is used to communicate with the DB about devices management via DBIO
 * 
 * @author hongzhen
 */

import java.util.*;

public class UserDevicesManager {

	@SuppressWarnings("unused")
	private User myUser; // myUser is only set in the constructor

	/**
	 * Construct UserDeviceManager that belongs to a specific user
	 * @param user
	 */
	public UserDevicesManager(User user) {
		myUser = user;
	}
	
	public List<Device> getDeviceList() {
		List<Device> deviceList = new ArrayList<Device>();
		//deviceList = DBIO.getDeviceList();
		return deviceList;
	}
	
	public void addDevice(Device device) { 
		//DBIO.deviceList.add(myUser.getUserEmail(), device);
	}
	
	public void deleteDevice(Device device) {
		//DBIO.deviceList.remove(myUser.getUserEmail(), device);
	}
	
	public boolean checkDevice(Device device) {
		List<Device> deviceList = new ArrayList<Device>();
		//deviceList = DBIO.getDeviceList;
		return deviceList.contains(device);
	}

}
