/**
 * UserRulesManager object belongs to a specific User.
 * UserRulesManager is used to communicate with the DB about rules management via DBIO
 * 
 * @author hongzhen
 */

import java.util.*;

public class UserRulesManager {
	
	@SuppressWarnings("unused")
	private User myUser; // myUser is only set in the constructor

	/**
	 * Construct UserRulesManager that belongs to a specific user
	 * @param user
	 */
	public UserRulesManager(User user) {
		myUser = user;
	}

	public List<Rule> getRuleList() {
		List<Rule> ruleList = new ArrayList<Rule>();
		//ruleList = DBIO.getRuleList;
		return ruleList;
	}

	public void addRule(Rule rule) { 
		//DBIO.ruleList.add(myUser.getUserEmail(), rule);
	}
	
	public void deleteRule(Rule rule) {
		//DBIO.ruleList.remove(myUser.getUserEmail(), rule);
	}
	
	public boolean checkRule(Rule rule) {
		List<Rule> ruleList = new ArrayList<Rule>();
		//ruleList = DBIO.getRuleList;
		return ruleList.contains(rule);
	}
	
}
