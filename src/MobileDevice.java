/**
 * MobileDevice interface contains abstract methods to be specified by mobile Devices:
 * e.g. iPhone 5, iPhone 4S, Samsung Galaxy S6...
 * @author hongzhen
 *
 */
public interface MobileDevice {
	boolean turnOnBluetooth();
	boolean turnOffBluetooth();
	int[] getGPSLocation();
//	boolean connectToDevice(Device device);
}
