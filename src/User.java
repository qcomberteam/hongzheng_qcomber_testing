/**
 * User object represents a specific user, it contains that user's identification and optional nickname
 * 
 * @author hongzhen
 */
public class User {
	private String userEmail;
	private String userPassword;
	private String userNickname; // This name is a nickname set by user, it is not used for identification
	
	/**
	 * Constructor when user's nickname is not given:
	 * Sets the user's email address and password (2 compulsory fields)
	 * @param email
	 * @param password
	 */
	public User(String email, String password) {
		userEmail = email;
		userPassword = password;
	}

	/**
	 * Constructor when user's nickname is given:
	 * Sets the user's email address, password, and nickname
	 * @param email
	 * @param password
	 * @param nickname
	 */
	public User(String email, String password, String nickname) {
		this(email,password);
		setUserNickname(nickname);
	}

	public String getUserEmail() {
		return userEmail;
	}
	
	public String changePassword(String oldPassword, String newPassword) {
		// Method used when a user wants to change password
		if (userPassword == oldPassword) { // must match current password to have the password changed
			if (newPassword != null) {
				userPassword = newPassword;
				return "Password Changed.";
			}
			return ("New password cannot be empty.");
		}
		else
			return "Input password does not match current password.";
	}

	public String getUserNickname() {
		return userNickname;
	}

	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}

}
