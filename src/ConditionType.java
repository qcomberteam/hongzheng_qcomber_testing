/**
 * Condition
 * @author hongzhen
 */

public class ConditionType {

	private String myConditionType;
	
	private String myName;
	
	
	public ConditionType(String conditionType) {
		setMyConditionType(conditionType);
	}
	
	public ConditionType(String conditionType, String conditionTypeName) {
		setMyConditionType(conditionType);
		setMyName(conditionTypeName);
	}

	public String getMyConditionType() {
		return myConditionType;
	}

	public void setMyConditionType(String myConditionType) {
		this.myConditionType = myConditionType;
	}

	public String getMyName() {
		return myName;
	}

	public void setMyName(String myName) {
		this.myName = myName;
	}

}
