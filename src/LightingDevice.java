/**
 * LightingDevice interface contains abstract methods to be specified by lighting Devices:
 * e.g. Philips HUE LED...
 * @author hongzhen
 *
 */
public interface LightingDevice {
	int[] changeColor(int[] RGB);
	int changeBrightness(int brightness);
	int autoTurnOff(int time);
}
