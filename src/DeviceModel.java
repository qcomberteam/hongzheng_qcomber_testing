/**
 * @author hongzhen
 *
 * DeviceModel Enum
 * 
 */

public enum DeviceModel {
	APPLE_IPHONE6, PHILIPS_HUELED;
	
	private String brand;
	private String model;
	
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
}
