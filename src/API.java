/**
 * @author hongzhen
 * 
 * An API object is specific for each DeviceModel, e.g. turn on HUE LED, 
 * turn off iPhone 6 Plus, turn on Panasonic microwave for some time, etc.
 * 
 * Specific API definition will be specified in each sub-class of Device.
 * deviceAPI should be pre-defined according to product APIs
 * 
 */

public class API {
	// Device class represents a specific device, i.e. William's Black iPhone 6 Plus

	private DeviceModel deviceModel;
	private String deviceAPI;
	
	/**
	 * Constructor
	 * @param deviceModel
	 * @param API
	 */
	public API(DeviceModel model, String API) {
		setDeviceModel(model);
		setDeviceAPI(API);
	}

	public DeviceModel getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(DeviceModel deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getDeviceAPI() {
		return deviceAPI;
	}

	public void setDeviceAPI(String deviceAPI) {
		this.deviceAPI = deviceAPI;
	}

}
